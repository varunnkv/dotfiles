setlocal shiftwidth=2
setlocal tabstop=2

let g:syntastic_c_include_dirs = [ '/usr/lib/openmpi/include']
