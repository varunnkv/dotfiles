import XMonad
import XMonad.Actions.SpawnOn
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeysP)
import System.IO
import XMonad.Hooks.SetWMName
import Control.Monad (liftM2)
import XMonad.Layout.NoBorders
import XMonad.Actions.GridSelect
import qualified XMonad.StackSet as W
{-myWorkspaces    = ["1","2","3","4","5","6","7","8","9"]-}
myWorkspaces    = ["term","web","ent","doc","rmt","misc","mty","mt2","hide"]
myLayout = tiled ||| Full
    where
        tiled   = Tall nmaster delta ratio
        nmaster = 1
        ratio   = 1/2
        delta   = 0.03
myTerminal = "urxvt"
myManageHook = composeAll
   [
       className =? "Firefox" --> doShift "web"
     , className =? "Chromium-browser" --> doShift "web"
     , (className =? "Firefox" <&&> resource =? "Dialog") --> doFloat
     , className =? "Gnome-mplayer" --> viewShift "ent"
     , className =? "Vlc" --> viewShift "ent"
     , className =? "mplayer2" --> viewShift "ent"
     {-, className =? "Gpicview" --> viewShift "doc"-}
     , className =? "com-mathworks-util-PostVMInit" --> viewShift "doc"
     , className =? "Zathura" --> viewShift "doc"
     , className =? "MATLAB" --> viewShift "doc"
     , className =? "Navigator" --> viewShift "5"
     , className =? "emacs" --> viewShift "doc"
     , className =? "Emacs24" --> viewShift "doc"
   ]
   where viewShift = doF . liftM2 (.) W.greedyView W.shift
{-myManageHook = composeAll-}
   {-[-}
       {-className =? "Firefox" --> doShift "2"-}
     {-, className =? "uzbl-core" --> doShift "2"-}
     {-, (className =? "Firefox" <&&> resource =? "Dialog") --> doFloat-}
     {-, className =? "Gnome-mplayer" --> viewShift "3"-}
     {-, className =? "Vlc" --> viewShift "3"-}
     {-, className =? "Gpicview" --> viewShift "4"-}
     {-, className =? "com-mathworks-util-PostVMInit" --> viewShift "4" -}
     {-, className =? "Zathura" --> viewShift "4" -}
     {-, className =? "MATLAB" --> viewShift "4" -}
     {-, className =? "Vidalia" --> viewShift "5" -}
     {-, className =? "Navigator" --> viewShift "5" -}
     {-, className =? "emacs" --> viewShift "4" -}
     {-, className =? "Emacs24" --> viewShift "4" -}
   {-]-}
   {-where viewShift = doF . liftM2 (.) W.greedyView W.shift-}
main = do
    xmproc <- spawnPipe "xmobar"
    xmonad $ defaultConfig
        {
        modMask = mod1Mask
        , normalBorderColor = "blue"
        , focusedBorderColor = "red"
        , borderWidth = 1
        , terminal = "urxvt"
        , workspaces = myWorkspaces
        -- , manageHook = manageDocks <+> manageHook defaultConfig
        , manageHook = manageDocks <+> myManageHook <+> manageHook defaultConfig
        , layoutHook = avoidStruts  $  smartBorders  $  myLayout
        , startupHook = startup >> setWMName "LG3D"
        , logHook = dynamicLogWithPP xmobarPP
            { ppOutput = hPutStrLn xmproc
            , ppSep = ""
            , ppCurrent = xmobarColor "#002b36" "#657b83" . wrap "" ""
            , ppLayout = xmobarColor "#002b36" "#002b36" . shorten 0
            , ppTitle = xmobarColor "#657b83" "" }
        }`additionalKeysP`
        [
        ("M-q", restart "xmonad" True)
        , ("M-b", sendMessage ToggleStruts)
        , ("M-i", goToSelected defaultGSConfig)
        , ("M-o", spawnSelected defaultGSConfig ["firefox", "emacs", "keynav", "slock"])
        ]
startup :: X() 
startup = do 
    spawnOn  "1" "urxvt"
    spawnOn  "3" "urxvt -e cmus"
    spawnOn  "2" "firefox"
    spawnOn  "4" "emacs ~/Programming/org/project.org"
