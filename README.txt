rm  /home/varun/.vimperatorrc
ln -s  /home/varun/Programming/Dotfiles/.vimperatorrc /home/varun/.vimperatorrc
rm  /home/varun/.bashrc
ln -s  /home/varun/Programming/Dotfiles/.bashrc /home/varun/.bashrc
rm  /home/varun/.zshrc
ln -s  /home/varun/Programming/Dotfiles/zshrc /home/varun/.zshrc
rm  /home/varun/.emacs
ln -s  /home/varun/Programming/Dotfiles/.emacs /home/varun/.emacs
rm  /home/varun/.Xdefaults
ln -s  /home/varun/Programming/Dotfiles/.Xdefaults /home/varun/.Xdefaults
rm  /home/varun/.xmonad/xmonad.hs
ln -s  /home/varun/Programming/Dotfiles/xmonad.hs /home/varun/.xmonad/xmonad.hs
ln -s  /home/varun/Programming/Dotfiles/urxvt /home/varun/.config/
ln -s  /home/varun/Programming/Dotfiles/z /home/varun/.config/z/
